/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/**
 ******************************************************************************
 * @file    Test_Array.cpp
 * @author  Ing. Manuel Alejandro Linares Paez
 * @version 1.0
 * @date    Mar 29, 2016
 * @brief   Test for a uint8_t array
 ******************************************************************************
 */
#include <gtest/gtest.h>

#include <cstring>

#include "Array.hpp"

using namespace eHSM;

class TestArrayInt8 : public ::testing::Test
{
 public:
  typedef int8_t                       ArrayType;
  static const std::size_t             ARRAY_LEN = 15;
  Declare::Array<ArrayType, ARRAY_LEN> testArray;
};

TEST_F(TestArrayInt8, EmptyAfterCreation)
{
  EXPECT_TRUE(testArray.isEmpty());
}

TEST_F(TestArrayInt8, CorrectMaxSize)
{
  EXPECT_EQ(15, testArray.size());
}

TEST_F(TestArrayInt8, AddElements)
{
  testArray.append(124);
  testArray.append(-89);
  testArray.append(23);
  EXPECT_EQ(testArray.at(0), 124);
  EXPECT_EQ(testArray.at(1), -89);
  EXPECT_EQ(testArray.at(2), 23);
  EXPECT_EQ(testArray.front(), 124);
  EXPECT_EQ(testArray.back(), 23);
  EXPECT_TRUE(!testArray.isEmpty());
  EXPECT_EQ(testArray.itemsAviable(), 3);
}

TEST_F(TestArrayInt8, CheckIndexOfElements)
{
  testArray.append(124);
  testArray.append(-89);
  testArray.append(23);
  EXPECT_EQ(testArray.indexOf(124), 0);
  EXPECT_EQ(testArray.indexOf(-89), 1);
  EXPECT_EQ(testArray.indexOf(23), 2);
}

TEST_F(TestArrayInt8, RemoveElements)
{
  testArray.append(124);
  testArray.append(-89);
  testArray.append(23);
  testArray.remove(0);
  EXPECT_EQ(testArray.at(0), -89);
  EXPECT_EQ(testArray.at(1), 23);
  EXPECT_TRUE(!testArray.isEmpty());
  EXPECT_EQ(testArray.itemsAviable(), 2);
}

TEST_F(TestArrayInt8, RemoveAllElements)
{
  testArray.append(124);
  testArray.append(-89);
  testArray.append(23);
  testArray.clear();
  EXPECT_TRUE(testArray.isEmpty());
}

TEST_F(TestArrayInt8, ChangeExistingElement)
{
  testArray.append(124);
  testArray.append(-89);
  testArray.append(23);
  testArray.at(0) = 45;
  EXPECT_EQ(testArray.at(0), 45);
}

TEST_F(TestArrayInt8, CheckFullArray)
{
  for(std::uint8_t i = 0; i < ARRAY_LEN - 1; i++)
  {
    testArray.append(i);
  }
  EXPECT_FALSE(testArray.isFull());
  testArray.append(23);
  EXPECT_TRUE(testArray.isFull());
}
