/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <cassert>
#include <cstdint>
#include <cstring>
#include <iterator>

#include "Utils.hpp"

namespace eHSM
{
/**
 * @brief The Array class
 * Provides a classic static array with a few modifications to its behavior.
 * You can only add new elements at the end, but can modify any existing
 * element. It keeps track of the amount of elements added. You can now the
 * position in the array of any element. You can remove elements.
 * @tparam Type Data type of the array.
 */
template <typename Type>
class Array
{
 public:
  /**
   * @brief itemsAviable
   * @return Number of items added to the array.
   */
  std::size_t itemsAviable() const
  {
    return _writeCount;
  }

  /**
   * @brief spaceAviable
   * @return Space left.
   */
  std::size_t spaceAviable() const
  {
    return size() - itemsAviable();
  }

  /**
   * @brief size
   * @return Maximum number of items that the array can contain.
   */
  std::size_t size() const
  {
    return _arraySize;
  }

  /**
   * @brief empty
   * @return
   * true if the array has no elements.
   * false if the array has at least one element.
   */
  bool isEmpty() const
  {
    return (_writeCount == 0);
  }

  /**
   * @brief full
   * @return
   * true if the array is full.
   * false if the array is not full.
   */
  bool isFull() const
  {
    return (_writeCount == _arraySize);
  }

  /**
   * @brief at
   * @param pos Position of the item in the array.
   * @return A reference to the item.
   */
  Type &at(std::size_t pos) const
  {
    assert(pos < _arraySize);
    assert(pos < _writeCount);
    return _arrayPtr[pos];
  }

  /**
   * @brief front
   * @return A reference to the first item in the array.
   */
  Type &front() const
  {
    assert(_writeCount > 0);
    return _arrayPtr[0];
  }

  /**
   * @brief back
   * @return A reference to the last item in the array.
   */
  Type &back() const
  {
    assert(_writeCount > 0);
    return _arrayPtr[_writeCount - 1];
  }

  /**
   * @brief Deletes all the elements in the array.
   */
  void clear()
  {
    _writeCount = 0;
  }

  /**
   * @brief Insert an item at the end of the array.
   * @param data Value to insert in the array.
   */
  bool append(const Type &data)
  {
    if(isFull())
    {
      return false;
    }
    _arrayPtr[_writeCount] = data;
    _writeCount++;
    return true;
  }

  /**
   * @brief none const iterator.
   */
  class iterator
  {
    public:
      typedef iterator self_type;
      typedef Type value_type;
      typedef Type& reference;
      typedef Type* pointer;
      typedef std::bidirectional_iterator_tag iterator_category;
      typedef int difference_type;
      iterator(pointer ptr) : ptr_(ptr) { }
      self_type operator++() { self_type i = *this; ptr_++; return i; }
      self_type operator++(int) { ptr_++; return *this; }
      self_type operator--() { self_type i = *this; ptr_--; return i; }
      self_type operator--(int) { ptr_--; return *this; }
      reference operator*() { return *ptr_; }
      pointer operator->() { return ptr_; }
      bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
      bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
    private:
      pointer ptr_;
  };

  /**
   * @brief const iterator.
   */
  class const_iterator
  {
    public:
      typedef const_iterator self_type;
      typedef Type value_type;
      typedef Type& reference;
      typedef Type* pointer;
      typedef int difference_type;
      typedef std::bidirectional_iterator_tag iterator_category;
      const_iterator(pointer ptr) : ptr_(ptr) { }
      self_type operator++() { self_type i = *this; ptr_++; return i; }
      self_type operator++(int) { ptr_++; return *this; }
      self_type operator--() { self_type i = *this; ptr_--; return i; }
      self_type operator--(int) { ptr_--; return *this; }
      const reference operator*() { return *ptr_; }
      const pointer operator->() { return ptr_; }
      bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
      bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
    private:
      pointer ptr_;
  };

  /**
   * @brief none const iterator begin.
   */
  iterator begin()
  {
      return iterator(_arrayPtr);
  }

  /**
   * @brief none const iterator end.
   */
  iterator end()
  {
      return iterator(_arrayPtr + _writeCount);
  }

  /**
   * @brief const iterator begin.
   */
  const_iterator begin() const
  {
      return const_iterator(_arrayPtr);
  }

  /**
   * @brief const iterator end.
   */
  const_iterator end() const
  {
      return const_iterator(_arrayPtr + _writeCount);
  }

  /**
   * @brief It gives the position of the first item iqual to data in the array.
   * The data rtpe must define the operator==.
   * @param data Item to find in the array.
   * @return Position of the item, pr -1 if not founded.
   */
  int indexOf(const Type &data) const
  {
    for(std::size_t i = 0; i < _writeCount; i++)
    {
      if(_arrayPtr[i] == data)
      {
        return i;
      }
    }
    return -1;
  }

  /**
   * @brief Deletes an item from the array.
   * @param index Position of the item to delete.
   */
  bool remove(std::size_t index)
  {
    assert(index < _arraySize - 1);
    assert(index < _writeCount);
    if((index > _arraySize - 1) || (index > _writeCount))
    {
      return false;
    }
    for(std::size_t i = index; i < _writeCount; i++)
    {
      _arrayPtr[i] = _arrayPtr[i + 1];
    }
    _writeCount--;
    return true;
  }

  /**
   * @brief data
   * @return Pointer to the internal array of elements.
   */
  Type *data()
  {
    return _arrayPtr;
  }

 protected:
  /**
   * @brief Array
   * Protected Constructor, for be used for the derived class.
   * @param _arrayPtr Pointer to the derived class internal array.
   * @param _arraySize Derived class array sizes.
   */
  Array(Type *arrayPtr, std::size_t arraySize) : _arrayPtr(arrayPtr), _arraySize(arraySize), _writeCount(0) {}

  Type *      _arrayPtr;
  std::size_t _arraySize;
  std::size_t _writeCount;

 protected:
  E_DISABLE_COPY(Array);

};  // Array

namespace Declare
{
/**
 * @brief Declare::Array class
 * Class used to instanciate the objects
 * @tparam Type Items data type, default to std::std::uint8_t
 * @tparam MAX_SIZE array size
 */
template <typename Type, std::size_t MAX_SIZE = 128>
class Array : public ::eHSM::Array<Type>
{
 public:
  /**
   * @brief Array
   * Public constructor.
   */
  Array() : eHSM::Array<Type>(_array, MAX_SIZE) {}

 private:
  Type _array[MAX_SIZE];
  E_DISABLE_COPY(Array);
};  // Array

}  // namespace Declare

}  // namespace eHSM

#endif  // ARRAY_HPP
