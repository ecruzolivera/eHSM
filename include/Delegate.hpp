/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/**
 ******************************************************************************
 * @file    Delegate.hpp
 * @author  Ing. Manuel Alejandro Linares Paez
 * @version 1.0
 * @date    Mar 28, 2016
 * @brief   Function delegate class
 * used to represent a member of a class as an object.
 * @bug     No known bugs.
 *
 ******************************************************************************
 */

#ifndef DELEGATE_HPP
#define DELEGATE_HPP

#include "Utils.hpp"

namespace eHSM
{
/**
 * @brief The Delegate class
 * An object of this class represents a member function of any other class.
 * The target function must have only one parameter of any type and return no
 * value.
 * @tparam Argument Function parameter type of the target function.
 */
template <typename Argument>
class Delegate
{
 public:
  /**
   * @brief Constructor initializes both private members to 0.
   */
  Delegate() : objectPtr(0), stubPtr(0) {}

  /**
   * @brief This member binds the target global or static function to this
   * Delegate object.
   *
   * @code
   * void globalFunction(int number){...}
   * .
   * .
   * .
   * Delegate functionObject;
   * functionObject.bind<&globalFunction>();
   * @endcode
   */
  template <void (*Method)(Argument)>
  void bind()
  {
    fromStub(0, &functionStub<Method>);
  }

  /**
   * @brief This member binds the target member function to this Delegate
   * object.
   *
   * @code
   * class ExampleClass
   * {
   * public:
   * void publicMember(int number){
   * printf("Hello wrold! %d",number);
   * }
   * };
   * .
   * .
   * ExampleClass obj;
   * Delegate functionObject;
   * functionObject.bind<ExampleClass,&ExampleClass::publicMember>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(Argument)>
  void bind(T *obj)
  {
    fromStub(obj, &functionStub<T, Method>);
  }

  /**
   * @brief This member binds the target const member function to this Delegate
   * object.
   *
   * @code
   * class ExampleClass
   * {
   * public:
   * void publicMember(int number)const{
   * printf("Hello wrold! %d",number);
   * }
   * };
   * .
   * .
   * ExampleClass obj;
   * Delegate functionObject;
   * functionObject.bind<ExampleClass,&ExampleClass::publicMember>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(Argument) const>
  void bind(T const *obj)
  {
    fromStub(const_cast<T *>(obj), &functionStub<T, Method>);
  }

  /**
   * @brief This function overloads operator () to simulate a regular function
   * call. Using this operator is equivalent to calling the function that the
   * object represents. If the Delegate is empty, this function does nothing
   * @param num This value is the parameter that takes the function represented.
   *
   * @code
   * ExampleClass obj;
   * Delegate functionObject;
   * functionObject.bind<ExampleClass,&ExampleClass::publicMember>(&obj);
   * .
   * .
   * functionObject(123);//This is the same that obj.publicMemeber(123);
   * @endcode
   */
  void operator()(Argument num) const
  {
    if(isUnbinded())
    {
      return;
    }
    return (*stubPtr)(objectPtr, num);
  }

  /**
   * @brief This function overloads operator ==. It determines if two Delegate
   * objects represent the same function.
   * @param dele The Delegate reference to compare.
   * @retval true if both Delegate objects represent the same function.
   * @retval false otherwise.
   */
  bool operator==(const Delegate &dele) const
  {
    return (objectPtr == dele.objectPtr) && (stubPtr == dele.stubPtr);
  }

  /**
   * @brief Unbinds the delegate from the function that previously targeted.
   */
  void clear()
  {
    objectPtr = 0;
    stubPtr   = 0;
  }

  bool isBinded() const
  {
    return !isUnbinded();
  }

  /**
   * @brief isUnbinded
   * @return If the delegate is binded to a target function.
   */
  bool isUnbinded() const
  {
    return (objectPtr == 0) & (stubPtr == 0);
  }

 private:
  /**
   * @brief Type definition for a pointer to a function that takes two
   * parameters, a pointer to void and another one defined by the template.
   */
  typedef void (*StubType)(void *, Argument);

  /**
   * @brief Pointer to the object of the class that contans the member function.
   */
  void *objectPtr;

  /**
   * @brief Pointer to the stub static function.
   */
  StubType stubPtr;

  /**
   * @brief Helper private member that assigns the object propertys.
   * @param objPtr Pointer to the object of the class that contans the member
   * function.
   * @param ptr Pointer to the stub function.
   */
  void fromStub(void *objPtr, StubType ptr)
  {
    objectPtr = objPtr;
    stubPtr   = ptr;
  }

  /**
   * @brief Stub static function for global and static functions.
   * @param ptr Pointer to the object of the class.
   * @param num Value of the parameter of the function represented.
   */
  template <void (*Method)(Argument)>
  static void functionStub(void *ptr, Argument num)
  {
    E_DECLARE_UNUSED(ptr);
    return (Method)(num);
  }

  /**
   * @brief Stub static function for member functions.
   * @param ptr Pointer to the object of the class.
   * @param num Value of the parameter of the function represented.
   */
  template <class T, void (T::*Method)(Argument)>
  static void functionStub(void *ptr, Argument num)
  {
    T *p = static_cast<T *>(ptr);
    return (p->*Method)(num);
  }

  /**
   * @brief Stub static function for const member functions.
   * @param ptr Pointer to the object of the class.
   * @param num Value of the parameter of the function represented.
   */
  template <class T, void (T::*Method)(Argument) const>
  static void functionStub(void *ptr, Argument num)
  {
    T const *p = static_cast<T *>(ptr);
    return (p->*Method)(num);
  }

};  // Delegate

/**
 * @brief Template specialization of the class
 * template<typename Argument>
 * class Delegate{}
 * This is for the special case when the function takes no arguments.
 */
template <>
class Delegate<void>
{
 public:
  /**
   * @brief Constructor initializes both private members to 0.
   */
  Delegate<void>() : objectPtr(0), stubPtr(0) {}

  /**
   * @brief This member binds the target global or static function to this
   * Delegate object.
   *
   * @code
   * void globalFunction(void){...}
   * .
   * .
   * .
   * Delegate functionObject;
   * functionObject.bind<&globalFunction>();
   * @endcode
   */
  template <void (*Method)(void)>
  void bind()
  {
    fromStub(0, &functionStub<Method>);
  }

  /**
   * @brief This member binds the target member function to this Delegate
   * object.
   *
   * @code
   * class ExampleClass
   * {
   * public:
   * void publicMember(void){
   * printf("Hello wrold!");
   * }
   * };
   * .
   * .
   * ExampleClass obj;
   * Delegate functionObject;
   * functionObject.bind<ExampleClass,&ExampleClass::publicMember>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(void)>
  void bind(T *obj)
  {
    fromStub(obj, &functionStub<T, Method>);
  }

  /**
   * @brief This member binds the target const member function to this Delegate
   * object.
   *
   * @code
   * class ExampleClass
   * {
   * public:
   * void publicMember(void)const{
   * printf("Hello wrold!");
   * }
   * };
   * .
   * .
   * ExampleClass obj;
   * Delegate functionObject;
   * functionObject.bind<ExampleClass,&ExampleClass::publicMember>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(void) const>
  void bind(T const *obj)
  {
    fromStub(const_cast<T *>(obj), &functionStub<T, Method>);
  }

  /**
   * @brief This function overloads operator () to simulate a regular function
   * call. Using this operator is equivalent to calling the function that the
   * object represents. If the Delegate is empty, this function does nothing
   * @param num This value is the parameter that takes the function represented.
   *
   * @code
   * ExampleClass obj;
   * Delegate functionObject;
   * functionObject.bind<ExampleClass,&ExampleClass::publicMember>(&obj);
   * .
   * .
   * functionObject();//This is the same that obj.publicMemeber();
   * @endcode
   */
  void operator()(void) const
  {
    if(isUnbinded())
    {
      return;
    }
    return (*stubPtr)(objectPtr);
  }

  /**
   * @brief This function overloads operator ==. It determines if two Delegate
   * objects represent the same function.
   * @param dele The Delegate reference to compare.
   * @retval true if both Delegate objects represent the same function.
   * @retval false otherwise.
   */
  bool operator==(const Delegate &dele) const
  {
    return (objectPtr == dele.objectPtr) && (stubPtr == dele.stubPtr);
  }

  /**
   * @brief Unbinds the delegate from the function that previously targeted.
   */
  void clear()
  {
    objectPtr = 0;
    stubPtr   = 0;
  }

  /**
   * @brief isUnbinded
   * @retval True if the delegate is bounded to a target function.
   */
  bool isBinded() const
  {
    return !isUnbinded();
  }
  /**
   * @brief isUnbinded
   * @retval True if the delegate is not bounded to a target function.
   */
  bool isUnbinded() const
  {
    return (objectPtr == 0) & (stubPtr == 0);
  }

 private:
  /**
   * @brief Type definition for a pointer to a function that takes one
   * parameters, a pointer to void.
   */
  typedef void (*StubType)(void *);

  /**
   * @brief Pointer to the object of the class that contans the member function.
   */
  void *objectPtr;

  /**
   * @brief Pointer to the stub static function.
   */
  StubType stubPtr;

  /**
   * @brief Helper private member that assigns the object propertys.
   * @param objPtr Pointer to the object of the class that contans the member
   * function.
   * @param ptr Pointer to the stub function.
   */
  void fromStub(void *objPtr, StubType ptr)
  {
    objectPtr = objPtr;
    stubPtr   = ptr;
  }

  /**
   * @brief Stub static function for global and static functions.
   * @param ptr Pointer to the object of the class.
   * @param num Value of the parameter of the function represented.
   */
  template <void (*Method)(void)>
  static void functionStub(void *ptr)
  {
    E_DECLARE_UNUSED(ptr);
    return (Method)();
  }

  /**
   * @brief Stub static function for member functions.
   * @param ptr Pointer to the object of the class.
   * @param num Value of the parameter of the function represented.
   */
  template <class T, void (T::*Method)(void)>
  static void functionStub(void *ptr)
  {
    T *p = static_cast<T *>(ptr);
    return (p->*Method)();
  }

  /**
   * @brief Stub static function for const member functions.
   * @param ptr Pointer to the object of the class.
   * @param num Value of the parameter of the function represented.
   */
  template <class T, void (T::*Method)(void) const>
  static void functionStub(void *ptr)
  {
    T const *p = static_cast<T *>(ptr);
    return (p->*Method)();
  }

};  // Delegate<void>

}  // namespace eHSM

#endif  // DELEGATE_HPP
