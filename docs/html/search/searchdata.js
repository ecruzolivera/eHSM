var indexSectionsWithContent =
{
  0: "_abcdefhimnoprstuw~",
  1: "abdehrs",
  2: "e",
  3: "abdhrsu",
  4: "abcdefhimnoprsw~",
  5: "_abdeinsw",
  6: "aes",
  7: "e",
  8: "et",
  9: "h",
  10: "eh",
  11: "be"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

